package com.example.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author rakeshr.ra
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class CustomerAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customerAccountId;
//	@OneToOne
//	@JoinColumn
	private Long customerId;
	private String currentAccountNumber;
	private double currentAccountNumberbalance;

}
