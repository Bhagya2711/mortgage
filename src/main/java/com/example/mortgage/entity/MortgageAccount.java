package com.example.mortgage.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author rakeshr.ra
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class MortgageAccount {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long mortgageAccountId;

	private Long customerId;
	private String mortgageAccountNumber;
	private double mortgageAccountNumberbalance;

}
