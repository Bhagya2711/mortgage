/**
 * 
 */
package com.example.mortgage.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author bhagyashree.naray
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class CustomerTransaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;
	
	private Long customerId;
	private String accountNumber;
	private Double transactionAmount;
	private LocalDateTime transactionDate;
	private String accountType;
	private String transactionType;


}
