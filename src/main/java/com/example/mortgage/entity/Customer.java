/**
 * 
 */
package com.example.mortgage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.example.mortgage.constant.ApplicationConstant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 * 
 */
@Data
@NoArgsConstructor
@Entity
@AllArgsConstructor
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long customerId;
	private String customerName;

	private String customerDob;

	 @Size(min=0,max=13,message =ApplicationConstant.PHONE_NUMBER_SHOULD_BE_LENGTH )

	private String phoneNumber;

	@Email
	@Column(unique = true)
	private String customerEmail;

	private String mortagagePurpose;
	private Double propertyCost;
	private Double deposit;
	private String occupation;
	private String contractType;
	private String employmentStatus;
	private String employmentDate;

	@Column(unique = true)
	private String customerLoginId;
	private String password;

	private String currentAccountNumber;
	private String mortgageAccountNumber;
	
	
	
	

}
