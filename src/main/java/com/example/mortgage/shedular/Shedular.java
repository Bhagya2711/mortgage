package com.example.mortgage.shedular;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.example.mortgage.service.TransactionServiceImpl;

import lombok.extern.slf4j.Slf4j;

/**
 * @author rakeshr.ra
 * @apiNote these is schedular class for transactionserviceimpl
 */
@Service
@Slf4j
public class Shedular {

	@Autowired
	TransactionServiceImpl transactionServiceImpl;

	@Scheduled(cron = "*/60 * * * * *")
	public void cronJobSch() {

		log.info("Java cron job expression {} ", LocalDateTime.now());
		transactionServiceImpl.startTranstaction();

	}

}
