/**
 * 
 */
package com.example.mortgage.service;

import java.util.List;

import javax.validation.Valid;

import com.example.mortgage.dto.FinalResponseDto;
import com.example.mortgage.exception.DuplicateEntryException;
import com.example.mortgage.request.RegisterRequest;
import com.example.mortgage.response.CustomerDto;

/**
 * @author bhagyashree.naray
 *
 */
public interface CustomerService {

	/**
	 * @param registerRequest
	 * @return
	 * @throws DuplicateEntryException 
	 */
	CustomerDto register(@Valid RegisterRequest registerRequest) throws DuplicateEntryException;

	/**
	 * @param customerId
	 * @return
	 * @throws DuplicateEntryException 
	 */
	List<FinalResponseDto> viewTransactions(Long customerId) throws DuplicateEntryException;

}
