/**
 * 
 */
package com.example.mortgage.service;

import org.springframework.stereotype.Service;

import com.example.mortgage.dto.LoginDto;
import com.example.mortgage.dto.ResponseDto;

/**
 * @author bhagyashree.naray
 *
 */
@Service
public interface LoginService {

	public ResponseDto userLogin(LoginDto loginDto);
}
