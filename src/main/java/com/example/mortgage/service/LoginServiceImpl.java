package com.example.mortgage.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mortgage.constant.ApplicationConstant;
import com.example.mortgage.dto.LoginDto;
import com.example.mortgage.dto.ResponseDto;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.exception.CustomerNotFound;
import com.example.mortgage.repository.CustomerRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
	@Autowired
	CustomerRepository customerRepository;

	@Override
	public ResponseDto userLogin(LoginDto loginDto) {
		log.info("UserServiceImpl: registerUser ");
		Optional<Customer> optionalCustomer = customerRepository
				.findByCustomerLoginIdAndPassword(loginDto.getCustomerLoginId(), loginDto.getPassword());
		if (!optionalCustomer.isPresent()) {
			throw new CustomerNotFound(ApplicationConstant.INVALID_LOGIN);
		}
		ResponseDto response = new ResponseDto();

		BeanUtils.copyProperties(optionalCustomer.get(), response);
		response.setMessage(ApplicationConstant.LOGIN_SUCCESS);
		response.setCode(200l);

		return response;

	}

}
