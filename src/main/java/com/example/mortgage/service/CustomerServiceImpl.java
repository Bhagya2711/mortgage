package com.example.mortgage.service;

 

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

 

import javax.validation.Valid;
import javax.validation.ValidationException;

 

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 

import com.example.mortgage.constant.ApplicationConstant;
import com.example.mortgage.dto.FinalResponseDto;
import com.example.mortgage.dto.TransactionDto;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.entity.CustomerAccount;
import com.example.mortgage.entity.CustomerTransaction;
import com.example.mortgage.entity.MortgageAccount;
import com.example.mortgage.exception.CustomerNotFound;
import com.example.mortgage.exception.DuplicateEntryException;
import com.example.mortgage.exception.InvalidAgeException;
import com.example.mortgage.repository.CustomerAccountRepo;
import com.example.mortgage.repository.CustomerRepository;
import com.example.mortgage.repository.CustomerTransactionRepository;
import com.example.mortgage.repository.MortageAccountRepo;
import com.example.mortgage.request.RegisterRequest;
import com.example.mortgage.response.CustomerDto;
import com.example.mortgage.utils.CommonUtils;

 

/**
 * @author bhagyashree.naray
 *
 */
@Service
public class CustomerServiceImpl implements CustomerService {

 

    private static final String CUSTOMER_ALREADY_REGISTERED = "Customer Already Registered";
    private static Long count = 0L;
    @Autowired
    CustomerRepository customerRepo;
    @Autowired
    CustomerAccountRepo customerAccountRepo;
    @Autowired
    MortageAccountRepo mortageAccountRepo;

 

    @Autowired
    CustomerTransactionRepository customerTransactionRepo;

 

    @Override
    public CustomerDto register(@Valid RegisterRequest registerRequest) throws DuplicateEntryException {

 

        StringBuilder fullName;
        Customer existingUser = customerRepo.findByCustomerEmail(registerRequest.getCustomerEmail());
        if (existingUser != null) {
            throw new DuplicateEntryException(CUSTOMER_ALREADY_REGISTERED);
        }

 

        LocalDate parseStringToDate = CommonUtils.parseStringToDate(registerRequest.getCustomerDob());

 

        Period period = Period.between(parseStringToDate, LocalDate.now());

 

        if (period.getYears() <= 17) {
            throw new InvalidAgeException(ApplicationConstant.USER_AGE);

 

        }

 

        if (registerRequest.getEmploymentStatus().equalsIgnoreCase("Employed")
                && registerRequest.getContractType() == null) {
            throw new ValidationException("Contract type and Employment date is mandatory for Employed customer");

 

        }

 

        Customer customer = new Customer();

 

        BeanUtils.copyProperties(registerRequest, customer);

 

        if (registerRequest.getMiddleName() != null) {
            fullName = new StringBuilder(registerRequest.getFirstName()).append(" ")
                    .append(registerRequest.getMiddleName()).append(" ").append(registerRequest.getLastName());
        } else {
            fullName = new StringBuilder(registerRequest.getFirstName()).append(" ")
                    .append(registerRequest.getLastName());
        }
        count++;
        customer.setCustomerName(fullName.toString());
        customer.setCustomerLoginId(new StringBuilder().append(registerRequest.getCustomerEmail().split("@")[0])
                .append(new Random().nextInt(30000)).toString());
        customer.setPassword(new StringBuilder().append("pass").append(new Random().nextLong()).toString());
        customer.setCurrentAccountNumber((new StringBuilder().append("C").append(count.toString()).toString()));
        customer.setMortgageAccountNumber(new StringBuilder().append("M").append(count.toString()).toString());

 

        Customer customerEntity = customerRepo.save(customer);

 

        Double price = customerEntity.getPropertyCost() - customerEntity.getDeposit();

 

        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setCurrentAccountNumber(customerEntity.getCurrentAccountNumber());
        customerAccount.setCustomerId(customerEntity.getCustomerId());
        customerAccount.setCurrentAccountNumberbalance(price);
        customerAccountRepo.save(customerAccount);

 

        // saving into mortageAccount entity
        MortgageAccount mortageAccount = new MortgageAccount();
        mortageAccount.setMortgageAccountNumber(customerEntity.getMortgageAccountNumber());
        mortageAccount.setCustomerId(customerEntity.getCustomerId());
        mortageAccount.setMortgageAccountNumberbalance(-price);

 

        mortageAccountRepo.save(mortageAccount);

 

        CustomerDto dto = new CustomerDto();
        BeanUtils.copyProperties(customerEntity, dto);
        return dto;

 

    }

 

    
    
    @Override
    public List<FinalResponseDto> viewTransactions(Long customerId) {
        
        Optional<Customer> existingUserOptional = customerRepo.findById(customerId);
        if (!existingUserOptional.isPresent()) {
            throw new CustomerNotFound("Customer Not Found");
        }
        Optional<CustomerAccount> custAcoountOpt =customerAccountRepo.findById(customerId);
        Optional<MortgageAccount> mortAcoountOpt =mortageAccountRepo.findById(customerId);
        
        
        
        List<FinalResponseDto> finalResponse = new ArrayList<>();
        FinalResponseDto response = new FinalResponseDto();
        List<TransactionDto> dtoList = new ArrayList<>();
        CustomerAccount object = new CustomerAccount();
        if(custAcoountOpt.isPresent()){
        object =    custAcoountOpt.get();
        response.setAccountNumber(object.getCurrentAccountNumber());
        response.setBalance(object.getCurrentAccountNumberbalance());
        }

 

        List<CustomerTransaction> currentTransactions = customerTransactionRepo
                .findTop5ByCustomerIdAndAccountNumberOrderByTransactionDateDesc(customerId, object.getCurrentAccountNumber());
        
        currentTransactions.forEach(temp -> {
            TransactionDto dto = new TransactionDto();
            BeanUtils.copyProperties(temp, dto);
            dtoList.add(dto);
            
        });
        
        response.setTransactionDtoList(dtoList);
        finalResponse.add(response);

 

        FinalResponseDto response2 = new FinalResponseDto();
        List<TransactionDto> dtoList2 = new ArrayList<>();
        MortgageAccount object1=new MortgageAccount();
        if (mortAcoountOpt.isPresent()) {
            object1 = mortAcoountOpt.get();
            response2.setAccountNumber(object1.getMortgageAccountNumber());
            response2.setBalance(object1.getMortgageAccountNumberbalance());
        }
        List<CustomerTransaction> currentTransactions2 = customerTransactionRepo
                .findTop5ByCustomerIdAndAccountNumberOrderByTransactionDateDesc(customerId, object1.getMortgageAccountNumber());
        
        currentTransactions2.forEach(temp -> {
            TransactionDto dto = new TransactionDto();
            BeanUtils.copyProperties(temp, dto);
            dtoList2.add(dto);
            
        });
        
        response2.setTransactionDtoList(dtoList2);
        finalResponse.add(response2);
        
        return finalResponse;

 

    }

 

}