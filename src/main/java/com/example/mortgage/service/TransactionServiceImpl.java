package com.example.mortgage.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mortgage.constant.ApplicationConstant;
import com.example.mortgage.entity.CustomerAccount;
import com.example.mortgage.entity.CustomerTransaction;
import com.example.mortgage.entity.MortgageAccount;
import com.example.mortgage.repository.CustomerAccountRepo;
import com.example.mortgage.repository.CustomerTransactionRepository;
import com.example.mortgage.repository.MortageAccountRepo;

/**
 * @author rakeshr.ra
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	MortageAccountRepo mortageAccountRepo;
	@Autowired
	CustomerAccountRepo customerAccountRepo;
	@Autowired
	CustomerTransactionRepository customerTransactionRepository;

	@Override
	public String startTranstaction() {

		List<MortgageAccount> findAll = mortageAccountRepo.findAll();
		
		findAll.forEach(temp -> {
			if (temp.getMortgageAccountNumberbalance() != 0) {
				/// adding to morgage
				Long customerId = temp.getCustomerId();
				double balance = temp.getMortgageAccountNumberbalance() + 1000;
				temp.setMortgageAccountNumberbalance(balance);
				mortageAccountRepo.save(temp);

				/// debiting from customer
				CustomerAccount customerAccount = customerAccountRepo.findByCustomerId(customerId);
				double price = customerAccount.getCurrentAccountNumberbalance() - 1000;
				customerAccount.setCurrentAccountNumberbalance(price);
				customerAccountRepo.save(customerAccount);

				CustomerTransaction customerTransaction = new CustomerTransaction();
				customerTransaction.setAccountType("customerAccount");
				customerTransaction.setTransactionAmount(1000.00);
				customerTransaction.setTransactionDate(LocalDateTime.now());
				customerTransaction.setCustomerId(customerId);
				customerTransaction.setTransactionType("debit");
				customerTransaction.setAccountNumber(customerAccount.getCurrentAccountNumber());
				customerTransactionRepository.save(customerTransaction);

				CustomerTransaction customerTransactions = new CustomerTransaction();
				customerTransactions.setAccountNumber(temp.getMortgageAccountNumber());
				customerTransactions.setAccountType("mortageAccount");
				customerTransactions.setAccountNumber(temp.getMortgageAccountNumber());
				customerTransactions.setTransactionAmount(1000.00);
				customerTransactions.setTransactionDate(LocalDateTime.now());
				customerTransactions.setCustomerId(customerId);
				customerTransactions.setTransactionType("credit");

				
				customerTransactionRepository.save(customerTransactions);

			}

		});
		return ApplicationConstant.SCHEDULAR_RUNNING;
		

	}

}
