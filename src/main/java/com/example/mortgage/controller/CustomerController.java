

package com.example.mortgage.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mortgage.dto.FinalResponseDto;
import com.example.mortgage.exception.DuplicateEntryException;
import com.example.mortgage.request.RegisterRequest;
import com.example.mortgage.response.CustomerDto;
import com.example.mortgage.service.CustomerService;

import io.swagger.annotations.ApiOperation;

/**
 * @author hemanth.garlapati
 * 
 */
@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CustomerController {
	
Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	

	
	/**Register customer
	 * @param registerRequest
	 * @return
	 * @throws DuplicateEntryException
	 */
	@ApiOperation(value = "Customer Registration")
	@PostMapping
	public ResponseEntity<CustomerDto> register(@Valid @RequestBody  RegisterRequest registerRequest) throws DuplicateEntryException  {
		logger.info("CustomerController: Inside register method");
		return new ResponseEntity<>(customerService.register(registerRequest), HttpStatus.OK);
	}
	
	
	/**View Transactions for a customer
	 * @param customerId
	 * @return
	 * @throws DuplicateEntryException 
	 */
	@ApiOperation(value = "View Transactions")
	@GetMapping("/{customerId}/transactions")
	public ResponseEntity<List<FinalResponseDto>> viewTransactions(@PathVariable("customerId") Long customerId ) throws DuplicateEntryException {
		logger.info("CustomerController: Inside viewTransactions method");
		return new ResponseEntity<>(customerService.viewTransactions(customerId), HttpStatus.OK);
	}
	


}
