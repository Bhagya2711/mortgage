/**
 * 
 */
package com.example.mortgage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.mortgage.dto.LoginDto;
import com.example.mortgage.dto.ResponseDto;
import com.example.mortgage.service.LoginService;

/**
 * @author hemanth.garlapati
 * 
 */
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LoginController {

	@Autowired
	LoginService loginService;
	Logger logger = LoggerFactory.getLogger(CustomerController.class);

	@PostMapping("/login")
	public ResponseEntity<ResponseDto> userLogin(@RequestBody LoginDto loginDto) {
		logger.info("LoginController: userLogin ");
		return  new ResponseEntity<>(loginService.userLogin(loginDto), HttpStatus.OK);
	}

}
