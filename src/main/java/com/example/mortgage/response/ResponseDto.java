/**
 * 
 */
package com.example.mortgage.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@AllArgsConstructor
public class ResponseDto {
	
	
		
		private String message;
		
		private Long statusCode;
	
}
