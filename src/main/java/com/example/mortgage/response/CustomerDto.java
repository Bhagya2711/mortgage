/**
 * 
 */
package com.example.mortgage.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {

	
	private Long customerId;
	
	private String customerName;
	
	private String customerLoginId;
	private String password;
	
	private String currentAccountNumber;
	private String mortgageAccountNumber;
	
	


}
