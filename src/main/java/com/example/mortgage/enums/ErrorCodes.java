/**
 * 
 */
package com.example.mortgage.enums;

/**
 * @author hemanth.garlapati
 *
 */
public enum ErrorCodes {
	USER_AGE(4041l), VALIDATION_FAILED(4001l), GENERIC_ERROR(5001l), DUPLICATE_ENTRY(4002l),TRIAL_VERSION_END(4022l),CUSTOMER_NOT_FOUND(404l);

	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}
