package com.example.mortgage.constant;

public class ApplicationConstant {

	private ApplicationConstant() {

	}

	public static final String PHONE_NUMBER_SHOULD_BE_LENGTH = "10";
	public static final String USER_AGE = "sorry cannot provide mortgage at the moment- Age is less than 18 years";
	public static final String INVALID_LOGIN = "Invalid user id or password, try again";
	public static final String LOGIN_SUCCESS = "Logged in Successfully";
	
	public static final String SCHEDULAR_RUNNING="SCHEDULAR RUNNING";

}
