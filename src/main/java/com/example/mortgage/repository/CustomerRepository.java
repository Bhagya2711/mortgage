/**
 * 
 */
package com.example.mortgage.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mortgage.entity.Customer;

/**
 * @author bhagyashree.naray
 *
 */
@Repository
public interface CustomerRepository  extends JpaRepository<Customer, Long>{

	/**
	 * @param customerEmail
	 * @return customer
	 */
	Customer findByCustomerEmail(String customerEmail);
	
	/**
	 * @param customerLoginId
	 * @param password
	 * @return customer
	 */
	Optional<Customer> findByCustomerLoginIdAndPassword(String customerLoginId,String password);

}
