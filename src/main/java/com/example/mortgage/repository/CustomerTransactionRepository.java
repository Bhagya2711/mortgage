/**
 * 
 */
package com.example.mortgage.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mortgage.entity.CustomerTransaction;


/**
 * @author bhagyashree.naray
 *
 */
@Repository
public interface CustomerTransactionRepository extends JpaRepository<CustomerTransaction, Long>{

	/**
	 * @param customerId
	 * @param string
	 */
	List<CustomerTransaction> findByCustomerIdAndAccountNumber(Long customerId, String string);

	/**
	 * @param customerId
	 * @param mortgageAccountNumber
	 * @return
	 */
	List<CustomerTransaction> findTop5ByCustomerIdAndAccountNumberOrderByTransactionDate(Long customerId,
			String mortgageAccountNumber);

	/**
	 * @param customerId
	 * @param mortgageAccountNumber
	 * @return
	 */
	List<CustomerTransaction> findTop5ByCustomerIdAndAccountNumberOrderByTransactionDateDesc(Long customerId,
			String mortgageAccountNumber);

}
