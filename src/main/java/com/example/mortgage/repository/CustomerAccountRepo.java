package com.example.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mortgage.entity.CustomerAccount;

/**
 * @author rakeshr.ra
 *
 */
@Repository
public interface CustomerAccountRepo extends JpaRepository<CustomerAccount, Long> {
	
CustomerAccount findByCustomerId(Long customerid);

}
