package com.example.mortgage.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.mortgage.entity.MortgageAccount;

/**
 * @author rakeshr.ra
 *
 */
@Repository
public interface MortageAccountRepo extends JpaRepository<MortgageAccount, Long> {

}
