/**
 * 
 */
package com.example.mortgage.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.example.mortgage.constant.ApplicationConstant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Component
@Data
@AllArgsConstructor

@NoArgsConstructor

public class RegisterRequest {

	@NotNull
	private String title;

	@NotNull(message = "Customer First Name is Mandatory")
	private String firstName;

	private String middleName;

	@NotNull(message = "Customer Last Name is Mandatory")
	private String lastName;

	private String customerDob;
	@Size(min = 0, max = 12, message = ApplicationConstant.PHONE_NUMBER_SHOULD_BE_LENGTH)

	private String phoneNumber;

	@Email(message = "Invalid email,please try again")
	private String customerEmail;

	@NotNull(message = "Operation Type is mandatory")
	private String mortagagePurpose;

	@NotNull(message = "property cost is Mandatory")
	@Min(value = 100000, message = "Property cost should be equal to more than Rs. 1lakh")
	private Double propertyCost;

	@NotNull(message = "deposit is Mandatory")
	@Min(value = 0, message = "Deposit should not be negative value")
	private Double deposit;

	@NotNull(message = "Employment status is mandatory")
	private String employmentStatus;

	private String occupation;
	private String contractType;
	private String employmentDate;

}
