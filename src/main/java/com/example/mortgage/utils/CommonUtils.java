package com.example.mortgage.utils;

/**
 * @author rakeshr.ra
 *
 */
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CommonUtils {

	/**
	 * 
	 */
	private CommonUtils() {
	}

	public static LocalDate parseStringToDate(String regDate) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		return LocalDate.parse(regDate, formatter);
	}

}
