/**
 * 
 */
package com.example.mortgage.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FinalResponseDto {
	
	private String accountNumber;
	private Double balance;
	List<TransactionDto> transactionDtoList;
	

}
