/**
 * 
 */
package com.example.mortgage.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author bhagyashree.naray
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDto {
	
	private Long transactionId;
	private Double transactionAmount;
	private LocalDateTime transactionDate;	
	private String accountType;
	private String transactionType;

}
