/**
 * 
 */
package com.example.mortgage.exception;

/**
 * @author hemanth.garlapati
 * 
 */
public class CustomerNotFound extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public CustomerNotFound(String message) {
		super(message);
	}

}
