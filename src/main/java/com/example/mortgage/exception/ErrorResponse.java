package com.example.mortgage.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponse {
	Long errorCode;
	List<String> errorMessage;
	

}
