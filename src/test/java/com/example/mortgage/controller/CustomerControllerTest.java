/**
 * 
 */
package com.example.mortgage.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.mortgage.dto.FinalResponseDto;
import com.example.mortgage.dto.TransactionDto;
import com.example.mortgage.exception.DuplicateEntryException;
import com.example.mortgage.request.RegisterRequest;
import com.example.mortgage.response.CustomerDto;
import com.example.mortgage.service.CustomerServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class CustomerControllerTest {

	@Mock
	CustomerServiceImpl customerServiceImpl;

	@InjectMocks
	CustomerController customerController;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testRegister() throws DuplicateEntryException{
		CustomerDto customerDto=new CustomerDto(1L, "hemanth", "hemanth1234", "hemanth1234","TR123", "MR123");
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", "sai", "garlapati", "07/07/2000",
				"8333852783", "hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent","date");
		Mockito.when(customerServiceImpl.register(registerRequest)).thenReturn(customerDto);
		assertEquals(200, customerController.register(registerRequest).getStatusCodeValue());

	}

    
	@Test
	    public void testviewTransactions() throws DuplicateEntryException{
	        
	        TransactionDto transactionDto=new TransactionDto(1l, 1000.0, LocalDateTime.now(), "customerAccount", "debit");
	        List<TransactionDto> transactionDtoList=new ArrayList<TransactionDto>();
	        transactionDtoList.add(transactionDto);
	        FinalResponseDto finalResponseDto=new FinalResponseDto("C1", 949000.0, transactionDtoList);
	        List<FinalResponseDto> dtos=new ArrayList<>();
	        dtos.add(finalResponseDto);
	        Mockito.when(customerServiceImpl.viewTransactions(1L)).thenReturn(dtos);
	        assertEquals(200, customerController.viewTransactions(1L).getStatusCodeValue());


	    }




}
