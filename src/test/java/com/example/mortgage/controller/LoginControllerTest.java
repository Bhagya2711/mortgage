/**
 * 
 */
package com.example.mortgage.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.mortgage.dto.LoginDto;
import com.example.mortgage.dto.ResponseDto;
import com.example.mortgage.service.LoginServiceImpl;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class LoginControllerTest {

	@Mock
	LoginServiceImpl loginServiceImpl;

	@InjectMocks
	LoginController loginController;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testUserLogin()  {
		LoginDto loginDto=new LoginDto("abc123", "abc123");
		ResponseDto responseDto=new ResponseDto(1L, "abc", "logged in successfully", 200l);
		Mockito.when(loginServiceImpl.userLogin(loginDto)).thenReturn(responseDto);
		assertEquals(200, loginController.userLogin(loginDto).getStatusCodeValue());

	}

}
