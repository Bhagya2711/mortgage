package com.example.mortgage.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.mortgage.constant.ApplicationConstant;
import com.example.mortgage.entity.CustomerAccount;
import com.example.mortgage.entity.MortgageAccount;
import com.example.mortgage.repository.CustomerAccountRepo;
import com.example.mortgage.repository.CustomerTransactionRepository;
import com.example.mortgage.repository.MortageAccountRepo;

@SpringBootTest
public class TransactionServiceImplTest {
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	@Mock
	MortageAccountRepo mortageAccountRepo;

	@Mock
	CustomerAccountRepo customerAccountRepo;
	@Mock
	CustomerTransactionRepository customerTransactionRepository;

	List<MortgageAccount> listMo = new ArrayList<MortgageAccount>();
	List<CustomerAccount> listCu = new ArrayList<CustomerAccount>();
	CustomerAccount co;


	@BeforeEach
	public void init() {
		MortgageAccount mo = new MortgageAccount(1l, 1l, "m1", 12000);
		listMo.add(mo);
		co = new CustomerAccount(1l, 1l, "m2", -1200);
		listCu.add(co);

	}

	@Test
	public void startTranstaction() {

		Mockito.when(mortageAccountRepo.findAll()).thenReturn(listMo);
		Mockito.when(customerAccountRepo.findByCustomerId(1l)).thenReturn(co);

		String startTranstaction = transactionServiceImpl.startTranstaction();
		Assertions.assertNotNull(startTranstaction);

	}

}
