
package com.example.mortgage.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.ValidationException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.mortgage.constant.ApplicationConstant;
import com.example.mortgage.dto.FinalResponseDto;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.entity.CustomerAccount;
import com.example.mortgage.entity.CustomerTransaction;
import com.example.mortgage.entity.MortgageAccount;
import com.example.mortgage.exception.CustomerNotFound;
import com.example.mortgage.exception.DuplicateEntryException;
import com.example.mortgage.exception.InvalidAgeException;
import com.example.mortgage.repository.CustomerAccountRepo;
import com.example.mortgage.repository.CustomerRepository;
import com.example.mortgage.repository.CustomerTransactionRepository;
import com.example.mortgage.repository.MortageAccountRepo;
import com.example.mortgage.request.RegisterRequest;
import com.example.mortgage.response.CustomerDto;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class CustomerServiceImplTest {

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;

	@Mock
	CustomerRepository customerRepository;
	
	@Mock
	CustomerAccountRepo customerAccountRepo;
	@Mock
	MortageAccountRepo mortageAccountRepo;
	

	@Mock
	CustomerTransactionRepository customerTransactionRepo;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testRegister() throws DuplicateEntryException {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", "sai", "garlapati", "07/07/2000",
				"8333852783", "hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent","07/07/2017");
		
				Customer customer = new Customer();
				customer.setCustomerId(1l);
				customer.setPropertyCost(100000d);
				customer.setDeposit(50000d);
				customer.setCurrentAccountNumber("C1234");
				customer.setMortgageAccountNumber("M1234");
				customer.setCustomerLoginId("C12");
				customer.setPassword("acb123");
				customer.setPhoneNumber(registerRequest.getPhoneNumber());
				
		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);
		
		assertEquals("acb123", customerServiceImpl.register(registerRequest).getPassword());
	}
	
	@Test
	public void testRegisterException() throws DuplicateEntryException{
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", "sai", "garlapati", "07/07/2000",
				"8333852783", "aa@gmail.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent","date");
		
				Customer customer = new Customer();
		BeanUtils.copyProperties(registerRequest, customer);
		Mockito.when(customerRepository.findByCustomerEmail(Mockito.any())).thenReturn(customer);
		assertThrows(DuplicateEntryException.class, () -> customerServiceImpl.register(registerRequest));
	}
	
    
	@Test
	    public void testRegisterDob() throws DuplicateEntryException {
	        try {
	            ArrayList<Customer> list = new ArrayList<Customer>();
	            RegisterRequest registerRequest = new RegisterRequest("Mr", "rakesh", "", "r", "07/07/2010", "8333852783",
	                    "rakesh@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent", "date");


	            Customer customer = new Customer();


	            Mockito.when(customerRepository.save(customer)).thenReturn(customer);
	            CustomerDto register = customerServiceImpl.register(registerRequest);
	        } catch (InvalidAgeException exception) {
	            assertEquals(ApplicationConstant.USER_AGE, exception.getLocalizedMessage());
	        }


	    }
	


	@Test
	public void testRegisterValidateException() throws DuplicateEntryException {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", "sai", "garlapati", "07/07/2000",
				"83338123783", "hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", null,
				"date");

		Customer customer = new Customer();
		BeanUtils.copyProperties(registerRequest, customer);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		assertThrows(ValidationException.class, () -> customerServiceImpl.register(registerRequest));
	}
	
	@Test
	public void testRegisterForNullContractType() throws DuplicateEntryException {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", "sai", "garlapati", "07/07/2000",
				"83338123783", "hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", null,
				"date");

		Customer customer = new Customer();
		BeanUtils.copyProperties(registerRequest, customer);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		assertThrows(ValidationException.class, () -> customerServiceImpl.register(registerRequest));
	}

	@Test
	public void testRegisterForNullMiddleName() throws DuplicateEntryException {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "hemanth", null, "garlapati", "07/07/2000",
				"8333852783", "hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent",
				"07/07/2017");

		Customer customer = new Customer();
		customer.setCustomerId(1l);
		customer.setPropertyCost(100000d);
		customer.setDeposit(50000d);
		customer.setCurrentAccountNumber("C1234");
		customer.setMortgageAccountNumber("M1234");
		customer.setCustomerLoginId("C12");
		customer.setPassword("acb123");
		customer.setPhoneNumber(registerRequest.getPhoneNumber());

		Mockito.when(customerRepository.save(Mockito.any(Customer.class))).thenReturn(customer);

		assertEquals("acb123", customerServiceImpl.register(registerRequest).getPassword());
	}
	
	@Test
	public void testViewTransactionException(){
		Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		assertThrows(CustomerNotFound.class, () -> customerServiceImpl.viewTransactions(2l));
		
	}
	
	@Test
	public void testViewTransaction() {
		
		Customer customer = new Customer();
		customer.setCustomerId(1l);
		customer.setPropertyCost(100000d);
		customer.setDeposit(50000d);
		customer.setCurrentAccountNumber("C1234");
		customer.setMortgageAccountNumber("M1234");
		customer.setCustomerLoginId("C12");
		customer.setPassword("acb123");
		Mockito.when(customerRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
		
		CustomerAccount custAcc = new CustomerAccount();
		custAcc.setCurrentAccountNumber("abc");
		custAcc.setCurrentAccountNumberbalance(10000d);
		custAcc.setCustomerAccountId(123l);
		custAcc.setCustomerId(1l);
		
		Mockito.when(customerAccountRepo.findById(1l)).thenReturn(Optional.of(custAcc));
		
		MortgageAccount mortAcc = new MortgageAccount();
		mortAcc.setMortgageAccountNumber("abc");
		mortAcc.setMortgageAccountNumberbalance(10000d);
		mortAcc.setMortgageAccountId(123l);
		mortAcc.setCustomerId(1l);
		
		
		Mockito.when(mortageAccountRepo.findById(1l)).thenReturn(Optional.of(mortAcc));
		
		CustomerTransaction trans = new CustomerTransaction();
		trans.setCustomerId(1l);
		trans.setTransactionAmount(1000d);
		List<CustomerTransaction> list = new ArrayList<>();
		Mockito.when(customerTransactionRepo.findTop5ByCustomerIdAndAccountNumberOrderByTransactionDate(Mockito.anyLong(), Mockito.anyString())).thenReturn(list);
		
		 List<FinalResponseDto> resp =customerServiceImpl.viewTransactions(1l);
		 assertEquals(2, resp.size());
		
	}
}
