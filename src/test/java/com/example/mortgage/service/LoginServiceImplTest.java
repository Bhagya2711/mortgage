/**
 * 
 */
package com.example.mortgage.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.mortgage.dto.LoginDto;
import com.example.mortgage.entity.Customer;
import com.example.mortgage.exception.CustomerNotFound;
import com.example.mortgage.repository.CustomerRepository;
import com.example.mortgage.request.RegisterRequest;

/**
 * @author hemanth.garlapati
 * 
 */
@SpringBootTest
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@BeforeEach
	public void init() {

	}

	@Test
	public void testLogin() {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "abc", "abc", "xyz", "07/07/2000", "83338123783",
				"hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent", "date");

		Customer customer = new Customer();
		BeanUtils.copyProperties(registerRequest, customer);
		customer.setCustomerLoginId("abc123");
		customer.setPassword("abc123");
		LoginDto loginDto = new LoginDto("abc123", "abc123");
		Mockito.when(customerRepository.findByCustomerLoginIdAndPassword("abc123", "abc123"))
				.thenReturn(Optional.of(customer));
		assertEquals(200, loginServiceImpl.userLogin(loginDto).getCode());

	}

	@Test
	public void testLoginCustomerNotFound() {
		RegisterRequest registerRequest = new RegisterRequest("Mr", "abc", "abc", "xyz", "07/07/2000", "83338123783",
				"hemanth@abc.com", "homeLoan", 10000000.0, 10000.0, "employed", "engineer", "permanent", "date");

		Customer customer = new Customer();
		BeanUtils.copyProperties(registerRequest, customer);
		customer.setCustomerLoginId("abc123");
		customer.setPassword("abc123");
		LoginDto loginDto = new LoginDto("abc123", "abc123");
		Mockito.when(customerRepository.findByCustomerLoginIdAndPassword("abc12345", "abc123"))
				.thenReturn(Optional.of(customer));
		assertThrows(CustomerNotFound.class, () -> loginServiceImpl.userLogin(loginDto));

	}

}
